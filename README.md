## Sitemap generator for Laravel
<p align="center">
<a href="https://packagist.org/packages/rustikjan/laravel-sitemap-generator"><img src="https://img.shields.io/packagist/dt/rustikjan/laravel-sitemap-generator" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/rustikjan/laravel-sitemap-generator"><img src="https://img.shields.io/packagist/v/rustikjan/laravel-sitemap-generator" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/rustikjan/laravel-sitemap-generator"><img src="https://img.shields.io/packagist/l/rustikjan/laravel-sitemap-generator" alt="License"></a>
</p>

### Install
```
composer require rustikjan/laravel-sitemap-generator
```
### Publish
```
php artisan vendor:publish --tag=sitemap-generator
```

### Config

```
make changes to the config/sitemap-generator.php
```

### Lang
support only json
```
php artisan vendor:publish --tag=sitemap-generator-lang
```
