<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="/vendor/sitemap-generator/sitemap.xsl"?>'; ?>
<urlset
    xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($sitemap as $item)
        <url>
            <loc>{{ url($item->url) }}</loc>
            <lastmod>{{ $item->lastmod }} </lastmod>
            <priority>{{ $item->priority }}</priority>
            <changefreq>{{ $item->changefreq }}</changefreq>
        </url>
    @endforeach
</urlset>
