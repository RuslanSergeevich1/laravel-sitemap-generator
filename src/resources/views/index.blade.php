<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="/vendor/sitemap-generator/sitemap.xsl"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($sitemap as $item)
        <sitemap>
            <loc>{{ url($item->url) }}</loc>
            <lastmod>{{ $item->lastmod }} </lastmod>
        </sitemap>
    @endforeach
</sitemapindex>

