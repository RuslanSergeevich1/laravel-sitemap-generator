<?php

namespace Rus\SitemapGenerator\Providers;

use Illuminate\Support\ServiceProvider;
use Orchid\Platform\Dashboard;
use Orchid\Platform\Providers\FoundationServiceProvider;

class SitemapGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/sitemap-generator.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'sitemap-generator');

        $this->publishes([
            __DIR__ . '/../config/sitemap-generator.php' => config_path('sitemap-generator.php'),
            __DIR__ . '/../public/' => public_path('vendor/sitemap-generator'),
        ], 'sitemap-generator');

        $this->publishes([
            __DIR__.'/../resources/lang' => lang_path('vendor/sitemap'),
        ], 'sitemap-generator-lang');
    }

    /**
     * Register translations.
     *
     * @return $this
     */
    public function registerTranslations(): self
    {
        $langPath = base_path('lang/vendor/sitemap');

        if (file_exists($langPath)) {
            $this->loadJsonTranslationsFrom($langPath);
        } else {
            $this->loadJsonTranslationsFrom(__DIR__ .'/../resources/lang/');
        }

        return $this;
    }

    /**
     * Register bindings the service provider.
     */
    public function register(): void
    {
        $this->registerTranslations();
    }
}
