<?php

return [
    /*
    |
    | This option defines parameters for the list of sitemaps and sitemap pages
    | For example, the 'pages' option determines the formation of a link for building a sitemap,
    | then the path to the list of pages in the sitemap index will be /sitemap/pages/sitemap.xml
    | For the 'articles' option : /sitemap/articles/sitemap.xml
    |
    | 'db_table' option determines from which table the list of pages will be obtained,
    | 'columns' which columns.
    | 'where_columns' to get active pages for example.
    |
    | Use 'prefix' option to change the link to page in the list of pages for example:
    | 'prefix' => 'page' : /page/new-page,
    | 'prefix' => 'article' :  /article/new-page,
    |
    | Example:
    |   'sitemap' => [
    |       'pages' => [
    |           'db_table' => 'pages',
    |           'columns' => ['slug', 'updated_at'],
    |           'where_columns' => ['is_published' => 1],
    |           'changefreq' => 'daily',
    |           'priority' => '0.8',
    |           'prefix' => '',
    |       ],
    |       'articles' => [],
    |   ],
    |
    */
    'sitemap' => [
        'pages' => [
            'db_table' => 'pages',
            'columns' => ['slug', 'updated_at'],
            'where_columns' => ['is_published' => 1],
            'changefreq' => 'daily',
            'priority' => '0.8',
            'prefix' => '',
        ],
        'articles' => [
            'db_table' => 'articles',
            'columns' => ['slug', 'updated_at'],
            'where_columns' => ['is_published' => 1],
            'changefreq' => 'weekly',
            'priority' => '0.9',
            'prefix' => '',
        ],
        'tags' => [
            'db_table' => 'tags',
            'columns' => ['slug', 'updated_at'],
            'where_columns' => ['is_published' => 1],
            'changefreq' => 'weekly',
            'priority' => '1.0',
            'prefix' => '',
        ],
    ],

    /*
     |
     | This option determines which column in the database by which data
     | will be sorted and retrieved for index sitemap page
     |
     */

    'last_updated_column' => 'updated_at',


    /*
     |
     | This option determines which column from the database will form links to pages in the sitemap.
     | The 'url' option must correspond to the link field in the database to the page
     |
     */

    'url' => 'slug',


    /*
     |
     | This option enables and disables page caching
     |
     */

    'cache' => false,

    /*
     |
     | This option configures the cache lifetime
     |
     */

    'expired_cache' => 3600 * 24 * 7, // seconds * hours * days


    /*
     |
     | This option defines the key for clearing the sitemap page cache.
     | To clear the cache, use the link /sitemap/clear-cache?key={clear_cache_key}
     |
     */

    'clear_cache_key' => '1db2e02999f251cc7a185029c7d59927375028e9'
];
