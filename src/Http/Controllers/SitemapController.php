<?php

namespace Rus\SitemapGenerator\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Throwable;

class SitemapController extends Controller
{
    private const EXPIRED = 3600 * 24 * 7;
    private const CHANGE_FREQ = 'weekly';
    private const PRIORITY = '1.0';
    private const OFFSET = 1000;

    public static function sitemap($page = null, $pagination = null): object
    {
        $config = config('sitemap-generator');
        $expired = $config['expired_cache'] ?? static::EXPIRED;

        if ($page) {
            if (!isset($config['sitemap'][$page]['db_table']) || !isset($config['sitemap'][$page]['columns'])) {
                return redirect('/sitemap.xml', 301);
            }

            if ($pagination) {
                if (isset($config['cache']) && $config['cache']) {
                    if (!isset(Cache::get('sitemap_cache')['groups'][$page]['pagination'][$pagination])) {
                        $dataByGroup = static::getSitemapDataByGroup($config, $page, $pagination);

                        $cache = Cache::get('sitemap_cache');
                        data_set($cache, 'groups.' . $page . '.pagination.' . $pagination, $dataByGroup);
                        Cache::put('sitemap_cache', $cache, $expired);

                    }
                    $sitemap = Cache::get('sitemap_cache')['groups'][$page]['pagination'][$pagination];
                } else {
                    $sitemap = static::getSitemapDataByGroup($config, $page, $pagination);
                }

                $view = 'sitemap-generator::sitemap';
            } else {
                if (isset($config['cache']) && $config['cache']) {
                    if (!isset(Cache::get('sitemap_cache')['groups'][$page]['index'])) {
                        $dataGroup = static::getSitemapDataGroup($config, $page);

                        $cache = Cache::get('sitemap_cache');
                        data_set($cache, 'groups.' . $page . '.index', $dataGroup);
                        Cache::put('sitemap_cache', $cache, $expired);

                    }
                    $sitemap = Cache::get('sitemap_cache')['groups'][$page]['index'];

                } else {
                    $sitemap = static::getSitemapDataGroup($config, $page);
                }

                $view = 'sitemap-generator::index';
            }

        } else {
            if (isset($config['cache']) && $config['cache']) {
                if (!isset(Cache::get('sitemap_cache')['index'])) {
                    $index = static::getSitemapIndex($config);

                    $cache = Cache::get('sitemap_cache');
                    data_set($cache,  'index', $index);
                    Cache::put('sitemap_cache', $cache, $expired);
                }
                $sitemap = Cache::get('sitemap_cache')['index'];
            } else {
                $sitemap = static::getSitemapIndex($config);
            }

            $view = 'sitemap-generator::index';
        }


        return response()->view($view, [
            'sitemap' => $sitemap,
        ])->header('Content-Type', 'text/xml');
    }

    private static function getSitemapIndex($config): array
    {
        $data = [];
        if (isset($config['sitemap'])) {
            foreach ($config['sitemap'] as $key => $item) {
                if (isset($item['db_table'])) {
                    $row[$key] = DB::table($item['db_table'])
                        ->where(function ($query) use ($item) {
                            if (isset($item['where_columns'])) {
                                foreach ($item['where_columns'] as $k => $v) {
                                    $query->where($k, $v);
                                }
                            }
                        })
                        ->orderBy($config['last_updated_column'],'DESC')
                        ->first($config['last_updated_column']);

                    if ($row[$key]) {
                        $data[$key] = (object) [
                            'lastmod' => get_object_vars($row[$key])[$config['last_updated_column']],
                            'url' => '/sitemap/'.$key.'/sitemap.xml'
                        ];
                    }
                }
            }
        }

        return $data;
    }

    private static function getSitemapDataGroup($config, $page): array
    {
        $count = DB::table($config['sitemap'][$page]['db_table'])
            ->where(function ($query) use ($config, $page) {
                if (isset($config['sitemap'][$page]['where_columns'])) {
                    foreach ($config['sitemap'][$page]['where_columns'] as $k => $v) {
                        $query->where($k, $v);
                    }
                }
            })->count();

        $c = (int) ceil($count / static::OFFSET);

        $rows = [];
        for ($i = 0; $i < $c; $i++) {
            $rows[$i] = DB::table($config['sitemap'][$page]['db_table'])
                ->where(function ($query) use ($config, $page) {
                    if (isset($config['sitemap'][$page]['where_columns'])) {
                        foreach ($config['sitemap'][$page]['where_columns'] as $k => $v) {
                            $query->where($k, $v);
                        }
                    }
                })
                ->offset($i * static::OFFSET)
                ->limit(static::OFFSET)
                ->orderBy('id')
                ->get($config['last_updated_column'])
                ->sortByDesc($config['last_updated_column'])
                ->toArray();

            $row = array_slice($rows[$i], 0, 1);
            $lastmod = get_object_vars($row[0])[$config['last_updated_column']];
            $rows[$i] = (object) [
                'lastmod' => $lastmod,
                'url' => '/sitemap/'.$page.'/sitemap-'.($i + 1).'.xml'
            ];
        }

        return array_reverse($rows);
    }


    private static function getSitemapDataByGroup($config, $page, $p): array
    {
        $data = [];
        $data['rows'] = DB::table($config['sitemap'][$page]['db_table'])
            ->where(function ($query) use ($config, $page) {
                if (isset($config['sitemap'][$page]['where_columns'])) {
                    foreach ($config['sitemap'][$page]['where_columns'] as $k => $v) {
                        $query->where($k, $v);
                    }
                }
            })
            ->offset($p * static::OFFSET - static::OFFSET)
            ->limit(static::OFFSET)
            ->orderBy('id')
            ->get($config['sitemap'][$page]['columns'])
            ->sortByDesc($config['last_updated_column']);

        $prefix = $config['sitemap'][$page]['prefix'] ?? '';
        $rows = [];

        foreach ($data['rows'] as $k => $row) {
            $rows[$k] = (object) [
                'lastmod' => get_object_vars($row)[$config['last_updated_column']],
                'url' => $prefix.get_object_vars($row)[$config['url']],
                'changefreq' => $config['sitemap'][$page]['changefreq'] ?? static::CHANGE_FREQ,
                'priority' => array_key_exists('priority', $config['sitemap'][$page])
                    ? $config['sitemap'][$page]['priority']
                    : static::PRIORITY,
            ];
        }

        return $rows;
    }

    public function clearCache(Request $request): object
    {
        $config = config('sitemap-generator');
        $key = $request->get('key');
        if ($key === $config['clear_cache_key']) {
            try {
                Cache::forget('sitemap_cache');
                return response()->json([
                    'status' => 'success',
                    'message' => __('Sitemap.xml updated'),
                ]);
            } catch (Throwable $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => $e
                ]);
            }
        }

        return redirect('/sitemap.xml');
    }
}
