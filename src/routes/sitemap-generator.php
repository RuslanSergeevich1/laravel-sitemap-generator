<?php

use Illuminate\Support\Facades\Route;
use \Rus\SitemapGenerator\Http\Controllers\SitemapController;


Route::get('/sitemap/clear-cache', [SitemapController::class, 'clearCache']);
Route::get('/sitemap.xml', [SitemapController::class, 'sitemap']);
Route::get('/sitemap/{page}/sitemap.xml', [SitemapController::class, 'sitemap']);
Route::get('/sitemap/{page}/sitemap-{p}.xml', [SitemapController::class, 'sitemap']);
